package com.unitsApi.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnitsApiApplication.class, args);
	}

}
