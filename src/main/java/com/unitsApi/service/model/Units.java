package com.unitsApi.service.model;

import java.util.List;

public class Units {
	
	protected String region;
	
	protected String total_cost;
	
	protected List<java.lang.String> machines;

	public Units(String region, int total_cost, List<java.lang.String> machinesNamesList) {
		super();
		this.region = region;
		this.total_cost = "$"+total_cost;
		this.machines = machinesNamesList;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTotal_cost() {
		return total_cost;
	}

	public void String(int total_cost) {
		this.total_cost = "$"+total_cost;
	}

	public List<java.lang.String> getMachines() {
		return machines;
	}

	public void setMachines(List<java.lang.String> machines) {
		this.machines = machines;
	}	
}
