package com.unitsApi.service.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitsApi.service.model.Units;

@RestController
public class UnitController {
	
	
	private int[] UNIT_SIZES = {320, 160, 80, 40, 20, 10};
	private int[] NY_PRICES = {2820,1400,774,450,230,120};
	private int[] INDIA_PRICES = {2970, 1300, 890, 413, 0, 140};
	private int[] CHINA_PRICES = {0, 1180, 670, 0, 200, 110};
	private String firstRegion = "New York", secondRegion = "India", thirdRegion = "China";
	
	private String[] UNIT_NAMES = {"10XLarge", "8XLarge", "4XLarge", "2XLarge", "XLarge", "Large"};
	
	//Main Service 
	@GetMapping("/units")
	public Units[] getUnitsPerHour(
			@RequestParam(name = "unitsAmount", required = true) int unitsAmount, 
			@RequestParam(name = "hours", required = true) int hours 
			) {
		
		int index = 3;
		//regions to calculate
		Units firstRegionData = calculatePrice(firstRegion,unitsAmount,hours);
		Units secondRegionData = calculatePrice(secondRegion,unitsAmount,hours);
		Units thirdRegionData = calculatePrice(thirdRegion,unitsAmount,hours);
		
		//to improve output, this optional calculation is required, this can be done with the others if needed
		Units scndRegionDataOpt = calculatePrice(secondRegion, unitsAmount, hours, index);
		
		//getting the total price without format  
		int fCost = Integer.parseInt( secondRegionData.getTotal_cost().substring( 1, secondRegionData.getTotal_cost().length()));
		int sCost = Integer.parseInt( scndRegionDataOpt.getTotal_cost().substring(1, scndRegionDataOpt.getTotal_cost().length()));
		
		//comparing both total cost, the first and the optional calculations
		if (fCost > sCost) {
			//if code gets here, means the optional calculation was lower than first calc
			// and is setting the lower total cost to send to the response
			secondRegionData = scndRegionDataOpt;
		}
		
		Units[] Output = {firstRegionData,secondRegionData,thirdRegionData};
		
		return Output;
	}
	
	/*
	 * Description:
	 * Method to calculate prices having the units amount and the hours
	 *  
	 * @Param String Region - Name of the region
	 * @Param int unitsAmount - Amount of units to sell
	 * @Param int hours - Requested hours for the units to work
	 * @Param int index - index to start the loop
	 * 
	 */
	private Units calculatePrice(String region ,int unitsAmount, int hours, int index) {
		List<String> machinesNamesList = new ArrayList<String>();
		List<Integer>  listOfIndex = new ArrayList<Integer>();;
		int totalCost = 0;
		int[] regionPrices = getRegionPrices(region);
		List<Integer> unitsIndex = calculateUnitCapacity(unitsAmount, index, listOfIndex, regionPrices);
		
		int currentNumber = 0;
		int counterIndex = 0;
		int totalIndex = 0;
		for (Integer i : unitsIndex) {
		
			if (currentNumber == 0 && counterIndex == 0) {
				currentNumber = i;
				counterIndex = 1;
			} else if ( currentNumber == i){
				counterIndex++;
			} else if (  currentNumber != i){
				machinesNamesList.add("("+UNIT_NAMES[currentNumber]+", " + (counterIndex) + ")");
				currentNumber = i;
				counterIndex = 1;
			}
			
			
			totalIndex ++;
			if ((totalIndex) == unitsIndex.size()){
				machinesNamesList.add("("+UNIT_NAMES[currentNumber]+", " + (counterIndex) + ")");
			}
			
					
			totalCost = totalCost + regionPrices[i];
		}
		
		totalCost = totalCost * hours;
		
		return new Units(region, totalCost, machinesNamesList);
	}
	/*
	 * Description:
	 * Method to calculate prices having the units amount and the hours
	 *  
	 * @Param String Region - Name of the region
	 * 
	 */
	private Units calculatePrice(String region,int unitsAmount, int hours ) {
		
		return calculatePrice(region, unitsAmount, hours, 1);
	}
	/*
	 * Description:
	 * Method to calculate the unit capacity with the unit value list, a counter to move to the next unit value and
	 * a indexList to track the index of the units to use 
	 *
	 * @Param int maxUnitCapacity - max unit capacity to reach
	 * @Param int index - index number for unit value array 
	 * @Param List<Integer>  indexList - list to track indexes
	 */
	 
	private List<Integer> calculateUnitCapacity(int maxUnitCapacity, int index, List<Integer> indexList, int[] regionPrices){
	
	
		if( regionPrices[index] == 0){
			return calculateUnitCapacity(maxUnitCapacity, index + 1 , indexList, regionPrices);
		} else { 
		
			if(maxUnitCapacity == 0) {
			
				return indexList;
				
			}else if (maxUnitCapacity >= UNIT_SIZES[index]) {
			
				indexList.add(index);
				return calculateUnitCapacity((maxUnitCapacity - UNIT_SIZES[index]), index, indexList, regionPrices);
				
			} else if (maxUnitCapacity < UNIT_SIZES[index]) {
			
				return calculateUnitCapacity(maxUnitCapacity, index + 1 , indexList, regionPrices);
			}
		}
		return indexList;
	}
	
	
	/*
	 * Description:
	 * Method to get the prices per region 
	 * 
	 * @Param String Region - Name of the region
	 * 
	 */
	private int[] getRegionPrices(String region) {
		int[] regionPrices;
		switch (region) {
		case "New York": {
			regionPrices = NY_PRICES;
			break;
		}
		case "India":{
			regionPrices = INDIA_PRICES;
			break;
		}
		default:
			regionPrices = CHINA_PRICES;
		}
		
		return regionPrices;
		
	}
}
