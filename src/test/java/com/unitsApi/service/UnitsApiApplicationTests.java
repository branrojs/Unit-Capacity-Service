package com.unitsApi.service;

import static org.assertj.core.api.Assertions.assertThatList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.unitsApi.service.controller.UnitController;
import com.unitsApi.service.model.Units;

@SpringBootTest
class UnitsApiApplicationTests {
	
	@Autowired
	UnitController uController;
	
	@Test
	public void testUnitsApi() {
		int unitsAmount= 1150;
		int hours = 1;
		Units [] OutPut = uController.getUnitsPerHour(unitsAmount, hours);
		assertNotNull(OutPut);
		
		String firstRegion = "New York";
		String fristCost = "$10150";
		Units NYData = OutPut[0];
		assertEquals(firstRegion, NYData.getRegion());
		assertEquals(fristCost, NYData.getTotal_cost());
		assertThatList(NYData.getMachines()).size().isGreaterThan(0);
		
		String secondRegion = "India";
		String secondCost = "$9520";
		Units IndiaData = OutPut[1];
		assertEquals(secondRegion, IndiaData.getRegion());
		assertEquals(secondCost, IndiaData.getTotal_cost());
		assertThatList(IndiaData.getMachines()).size().isGreaterThan(0);
		
		
		String thirdRegion = "China";
		String thirdCost = "$8570";
		Units ChinaData = OutPut[2];
		assertEquals(thirdRegion, ChinaData.getRegion());
		assertEquals(thirdCost, ChinaData.getTotal_cost());
		assertThatList(ChinaData.getMachines()).size().isGreaterThan(0);
		
		
	}

}